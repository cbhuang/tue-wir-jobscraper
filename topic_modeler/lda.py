import pymongo
from pymongo import MongoClient, CursorType
import pprint
import math
from pprint import pprint
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
import gensim
from gensim import corpora
import numpy as np
import matplotlib.pyplot as plt


"""
data retrieval
==================
"""

# query
client = MongoClient('mongodb://localhost:27017/')
db = client["tue_wir"]
collection = db["nvb_preprocessed"]
ls_corpus = []
for doc in collection.find({"tags.detailIsDutch": False,
                            "tags.keywords": "data analyst"},
                           {'detailCorpus': True},
                           cursor_type=CursorType.EXHAUST):
    ls_corpus.append(doc["detailCorpus"])

# subset take random

# random sampling, at most 100 files
n = 100
if len(ls_corpus) > n:
    ls_corpus = np.random.choice(ls_corpus, n, replace=False)
    print(f"[Info] {n} of {len(ls_corpus)} documents sampled")
else:
    print(f"[Info] {len(ls_corpus)} documents, no sampling")

"""
Generate document matrix
==========================
"""

stop = set(stopwords.words('english') + stopwords.words('dutch'))

exclude = set(string.punctuation)
lemma = WordNetLemmatizer()

# Creating the term dictionary of our corpus, where every unique term is assigned an index.
ls_tokens = []
for corpus in ls_corpus:
    # remove stopwords
    tokens = nltk.word_tokenize(corpus)
    new_tokens = [w for w in tokens if w not in stop]
    # return tokenized results
    ls_tokens.append(new_tokens)

dictionary = corpora.Dictionary(ls_tokens)

# Converting list of documents (corpus) into Document Term Matrix using dictionary prepared above.
doc_term_matrix = [dictionary.doc2bow(i) for i in ls_tokens]
#doc_term_matrix = dictionary.doc2bow(ls_corpus)

"""
LDA
==================
"""

# Creating the object for LDA model using gensim library
Lda = gensim.models.ldamodel.LdaModel

# Running and Trainign LDA model on the document term matrix.
ldamodel = Lda(doc_term_matrix, num_topics=5, id2word=dictionary, passes=1000)

ldamodel.print_topics(num_topics=5, num_words=10)

"""
Export for vis
==================
"""

ldamodel.save("out/ldamodel_en")
# csv??

"""
Visualize
==================
"""

# plot top 3 topics
for i in range(3):
    fig = plt.figure(figsize=(8, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)
    ls = ldamodel.show_topic(i)
    x = list(map(lambda k: k[0], ls))
    y = list(map(lambda k: k[1], ls))
    ax.bar(x, y)
    # title
    ax.set_title(f"Topic {i}", fontsize=18)
    # rotate text figure
    ax.tick_params(axis="x", rotation=45, size=16)
    #fig.tight_layout()
    fig.show()
    # save figure
    fig.savefig(f"out/lda_topic_{i}.png")
    # save csv
    with open(f'out/lda_topic_{i}.csv', 'w') as f:
        ls_lines = []
        for row in ls:
            ls_lines.append(f"{row[0]},{row[1]}\n")
        f.writelines(ls_lines)

    plt.close(fig)
