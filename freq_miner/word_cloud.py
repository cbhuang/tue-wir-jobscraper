"""
Topic Modeling

author: ....
date:
version: ....
"""

import pymongo
from pymongo import MongoClient, CursorType
import pprint
import math
from pprint import pprint
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
import gensim
from gensim import corpora
import numpy as np
# vis
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
from nltk.stem import WordNetLemmatizer

"""
Parameters
============
"""
# search_kw = "data engineer"
# isDutch = True


for search_kw in ["data analyst", "data engineer", "business analyst", "statistical programmer"]:
    for isDutch in [False, True]:
        if isDutch:
            lang = "Dutch"
        else:
            lang = "English"
        """
        data retrieval
        ==================
        """

        # query
        client = MongoClient('mongodb://localhost:27017/')
        db = client["tue_wir"]
        collection = db["nvb_preprocessed"]
        ls_corpus = []
        for doc in collection.find({"tags.detailIsDutch": isDutch,
                                    "tags.keywords": search_kw},
                                   {'detailCorpus': True},
                                   cursor_type=CursorType.EXHAUST):
            ls_corpus.append(doc["detailCorpus"])

        # subset take random

        # random sampling, at most 100 files (debug)
        n = 100
        if len(ls_corpus) > n:
            ls_corpus = np.random.choice(ls_corpus, n, replace=False)
            print(f"[Info] {n} of {len(ls_corpus)} documents sampled")
        else:
            print(f"[Info] {len(ls_corpus)} documents, no sampling")

        """
        ,                                                                               ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, , Generate document matrix
        ==========================
        """

        stop = set(stopwords.words('english') + stopwords.words('dutch'))

        exclude = set(string.punctuation)
        lemma = WordNetLemmatizer()

        # Creating the term dictionary of our corpus, where every unique term is assigned an index.
        ls_tokens = []
        for corpus in ls_corpus:
            # remove stopwords
            tokens = nltk.word_tokenize(corpus)
            new_tokens = [w for w in tokens if w not in stop]
            # return tokenized results
            ls_tokens.append(new_tokens)
        #
        # dictionary = corpora.Dictionary(ls_tokens)
        #
        # # Converting list of documents (corpus) into Document Term Matrix using dictionary prepared above.
        # doc_term_matrix = [dictionary.doc2bow(i) for i in ls_tokens]
        # #doc_term_matrix = dictionary.doc2bow(ls_corpus)
        #
        # """
        # LDA
        # ==================
        # """
        #
        # # Creating the object for LDA model using gensim library
        # Lda = gensim.models.ldamodel.LdaModel
        #
        # # Running and Trainign LDA model on the document term matrix.
        # ldamodel = Lda(doc_term_matrix, num_topics=5, id2word=dictionary, passes=200)
        #
        # ldamodel.print_topics(num_topics=5, num_words=10)

        """
        Export for vis
        ==================
        """
        #
        # ldamodel.save("out/ldamodel_en")
        # # csv

        """
        Vis
        ==================
        """


        # data = WordNetLemmatizer().lemmatize(ls_corpus, pos='v')
        data = ls_corpus

        wordcloud = WordCloud(
            background_color='white',
            stopwords=stop,
            max_words=200,
            max_font_size=40,
            scale=3,
            random_state=1  # chosen at random by flipping a coin; it was heads
        ).generate(str(data))

        fig = plt.figure(figsize=(12, 8), dpi=100)
        plt.axis('off')
        fig.suptitle(f"{search_kw}({lang})", fontsize=30)
        fig.subplots_adjust(top=2.3)

        plt.imshow(wordcloud)
        fig.show()
        fig.savefig(f"out/{search_kw}({lang}).png")
        plt.close()
