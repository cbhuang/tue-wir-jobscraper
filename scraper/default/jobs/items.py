# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NvbAggregateItem(scrapy.Item):
    """Aggregate stats provided by the website"""
    keyword = scrapy.Field()  # search keyword
    timestamp = scrapy.Field()  # timestamp
    total = scrapy.Field()  # total count
    careerLevel = scrapy.Field()
    category = scrapy.Field()
    companyType = scrapy.Field()
    contractType = scrapy.Field()
    educationLevel = scrapy.Field()
    industry = scrapy.Field()

    def __repr__(self):
        """For simple log printout"""
        return repr({"total": self["total"],
                     "keyword": self["keyword"],
                     "timestamp": self["timestamp"]})


class NvbItem(scrapy.Item):
    """raw simple information"""
    # Identifier
    docID = scrapy.Field()  # unique docID = site_postID
    # Search Info
    keywords = scrapy.Field()
    timestamp = scrapy.Field()
    # Raw Data (has complex structure)
    simpleData = scrapy.Field()
    sidebarData = scrapy.Field()
    detailData = scrapy.Field()

    def __repr__(self):
        """For simple log printout"""
        return repr({"docID": self["docID"],
                     "title": self["simpleData"]["title"],
                     "keywords": self["keywords"],
                     "timestamp": self["timestamp"]})
