# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from pymongo import MongoClient
from .items import *
from .settings import JSON_OUT_DIR, NVB_RAW, NVB_AGGR_RAW
#import json
#from scrapy.exporters import JsonItemExporter
#import os
#import numpy.random as rnd


class MongoPipeline(object):
    """Base class for all mongodb-related operations.

    Not to be used directly.
    """

    def __init__(self, host, port, db):
        self.host = host
        self.port = port
        self.dbName = db
        self.client = None
        self.db = None
        # collection is given by each decendant class

    @classmethod
    def from_crawler(cls, crawler):
        """Pass connection parameters into constructor"""
        return cls(
            crawler.settings.get('MONGO_HOST'),
            crawler.settings.get('MONGO_PORT'),
            crawler.settings.get('MONGO_DB'),
        )

    def open_spider(self, spider):
        # create connection
        self.client = MongoClient(host=self.host, port=self.port)
        # connect to db
        self.db = self.client[self.dbName]

    def close_spider(self, spider):
        self.client.close()


class NvbAggregatePipeline(MongoPipeline):
    """Aggregate statistics for NVB keyword search"""

    def process_item(self, item, spider):
        # type check
        if not isinstance(item, NvbAggregateItem):
            return item

        # upsert result (update when exist; insert when not)
        self.db[NVB_AGGR_RAW].replace_one({"keyword": item["keyword"]},
                                          dict(item),
                                          upsert=True)
        return item


class NvbPipeline(MongoPipeline):
    """Dump simple info to json file (for debugging)"""

    def process_item(self, item, spider):
        # type check
        if not isinstance(item, NvbItem):
            return item

        self.db[NVB_RAW].insert_one(dict(item))

        return item

