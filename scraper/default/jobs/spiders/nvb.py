import scrapy
from ..items import *
from ..settings import MAX_PAGES, SEARCH_KEYWORDS, MONGO_HOST, MONGO_PORT, MONGO_DB, NVB_RAW
from urllib.parse import urlencode
import json
from datetime import datetime
import os
from pymongo import MongoClient

class NvbSpider(scrapy.Spider):
    """
    NVB job description scraper

    The workflow is concatenated by callback functions: parse -> parse10
    -> parse20 -> ... . A separation point is created whenever a request
    is made.
    """

    name = 'nvb'
    start_urls = ["http://www.nationalevacaturebank.nl"]

    def __init__(self):
        # init parent class first
        super(NvbSpider, self).__init__()
        # mongoDB connection
        self.client = MongoClient(host=MONGO_HOST, port=MONGO_PORT)
        self.db = self.client[MONGO_DB]

    def __del__(self):
        self.client.close()

    def parse(self, response):
        """accept cookies"""
        link = "https://www.nationalevacaturebank.nl/?policy=accepted&utm_referrer="
        yield scrapy.Request(link, callback=self.parse10)

    def parse10(self, response):
        """search keywords"""
        if response.status != 200:
            print("[Warning] Bad parse10 initial state!")

        link = "https://www.nationalevacaturebank.nl/vacature/zoeken.json?"

        # search every keyword
        for kw in SEARCH_KEYWORDS:
            params = urlencode({'query': kw,
                                'location': "",
                                'distance': "",
                                'city': "",
                                'page': 1,
                                'limit': 10,
                                'sort': 'relevance'})
            request = scrapy.Request(link + params, callback=self.parse20)
            request.meta['keyword'] = kw  # pass over the search keyword to the next step
            yield request

    def parse20(self, response):
        """
        Process the first response from keyword search.
        Store aggregate information
        Luckily, the response is JSON for the NVB website.
        """

        # raw data
        data = json.loads(response.body_as_unicode())
        kw = response.meta['keyword']
        t = datetime.now().timestamp()
        page = data["result"]["pagination"]["page"]
        pages = data["result"]["pagination"]["pages"]

        # dump json (debug)
        # fname = os.path.join(JSON_OUT_DIR,
        #                      f"simple_{kw}_time={t}.json")
        # with open(fname, 'w') as f:
        #     json.dump(data, f)

        # 1. save aggregate stats
        if page == 1:
            item = NvbAggregateItem()
            item["keyword"] = kw
            item["timestamp"] = t
            item["total"] = data["result"]["total"]
            item["careerLevel"] = data["aggregations"][0]["buckets"]
            item["category"] = data["aggregations"][1]["buckets"]
            item["companyType"] = data["aggregations"][2]["buckets"]
            item["contractType"] = data["aggregations"][3]["buckets"]
            item["educationLevel"] = data["aggregations"][4]["buckets"]
            item["industry"] = data["aggregations"][5]["buckets"]
            yield item

        # 2. save jobs
        # Identifier
        for job in data["result"]["jobs"]:

            # dump json (debug)
            # fname = os.path.join(JSON_OUT_DIR,
            #                      f"simple_job_{i}_time={datetime.now().timestamp()}.json")
            # with open(fname, 'w') as f:
            #     json.dump(job, f)

            # create simple info item
            item = NvbItem()
            item["docID"] = f'NVB_{job["id"]}'
            item["keywords"] = [kw]  # this is a list
            item["timestamp"] = t
            item["simpleData"] = job

            # 3. if not scraped, retrieve detail info
            doc = self.db[NVB_RAW].find_one({"docID": item["docID"]})
            if doc is None:
                link = "https://www.nationalevacaturebank.nl" + job["detailUrl"]
                request = scrapy.Request(link, callback=self.parse30)
                request.meta["item"] = item  # bring to next step!
                yield request
            else:
                # if scraped, append this keyword, no repetition
                s = set().union(doc["keywords"], item["keywords"])  # prevent duplication
                self.db[NVB_RAW].update_one({"docID": doc["docID"]},
                                            {'$set': {'keywords': list(s)}})

        # recurse
        if page < min(pages, MAX_PAGES):
            page += 1
            link = "https://www.nationalevacaturebank.nl/vacature/zoeken.json?"
            params = urlencode({'query': kw,
                                'location': "",
                                'distance': "",
                                'city': "",
                                'page': page,
                                'limit': 10,
                                'sort': 'relevance'})
            request = scrapy.Request(link + params, callback=self.parse20)
            request.meta['keyword'] = kw  # pass over the search keyword to the next step
            yield request

    def parse30(self, response):
        """Retrieve detiled info"""

        item = response.meta["item"]

        # sidebar description
        sidebar_css = "#jobdetail > div.page-wrapper > main > article > div.sidebar > aside.ui-job-info > div > dl:nth-child({})"
        item["sidebarData"] = {
            "category": response.css(sidebar_css.format(2)).getall(),
            "titles": response.css(sidebar_css.format(3)).getall(),
            "industry": response.css(sidebar_css.format(4)).getall(),
            "contractType": response.css(sidebar_css.format(5)).getall(),
            "workingHours": response.css(sidebar_css.format(6)).getall(),
            "educationLevel": response.css(sidebar_css.format(7)).getall(),
            "careerLevel": response.css(sidebar_css.format(8)).getall(),
            "salary": response.css(sidebar_css.format(9)).getall()
        }

        # detailed description (HTML)
        item["detailData"] = response.css("#jobdetail > div.page-wrapper > main > article > div.content").get()

        yield item
