#!/bin/bash
# run script

# start Splash and record docker process ID
splash_psid=$(docker run -d -p 8050:8050 scrapinghub/splash)
# check exit status
if [ "$?" -ne "0" ]; then
  echo "[Error] splash: docker init failure!"
  exit 1
fi

# main crawler
scrapy crawl nvb

# stop splash
docker stop $splash_psid
