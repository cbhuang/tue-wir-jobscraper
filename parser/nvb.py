"""
Raw file preprocessor
"""

import xml
from pymongo import MongoClient, CursorType
from conf import *
import json
from bs4 import BeautifulSoup, NavigableString
from pprint import pprint
import re
import string
from textblob import TextBlob

class NvbParser(object):
    """
    Parse NVB_RAW based on schema
    """

    def __init__(self):
        # database
        self.host = MONGO_HOST
        self.port = MONGO_PORT
        self.dbName = MONGO_DB
        self.client = MongoClient(host=self.host, port=self.port)
        self.db = self.client[self.dbName]
        # data
        self.rawData = []  # list of raw documents
        self.clearData = []
        # keep the contents of these tags only
        self.keepTags = KEEP_TAGS

    def __del__(self):
        self.client.close()

    def get_mongo(self, limit=0):
        """Retrieve raw data from MongoDB"""
        # find record excluding _id (since it is not portable)
        if limit == 0:
            for doc in self.db[NVB_RAW].find({}, {'_id': False}, cursor_type=CursorType.EXHAUST):
                self.rawData.append(doc)
        else:
            for doc in self.db[NVB_RAW].find({}, {'_id': False}, limit=limit):
                self.rawData.append(doc)

    def write_mongo(self):
        """Write clear data back to mongodb"""
        try:
            self.db[NVB_PREPROCESSED].insert_many(self.clearData)
        except Exception as e:
            print(f"[Error] Failed to write clearData to mongodb: {str(e)}")
            raise e

    def preprocess(self):
        """Main preprocession function"""

        for doc in self.rawData:

            docID = doc['docID']

            # 1. parse HTML tree (the source may be invalid)
            try:
                soup1 = BeautifulSoup(doc["simpleData"]["description"], "lxml")
                soup2 = BeautifulSoup(doc["detailData"], "lxml")

            except RuntimeError as e:
                # dumping these IDs for special treatment
                with open(f"{OUT_DIR}/{LOGFILE}", "a") as f:
                    f.write(f"HTML parsing failed: docID={doc['docID']}\n")

                print(f"[Error] HTML parsing of docID={docID} failed: {str(e)}")

            # 2. remove unwanted html tags
            str1 = self.extract_html_content(soup1)
            str2 = self.extract_html_content(soup2)

            # 3. remove control characters
            str1 = self.remove_control_characters(str1)
            str2 = self.remove_control_characters(str2)

            # 4. remove punctuations
            str1 = self.remove_punctuations(str1)
            str2 = self.remove_punctuations(str2)

            # 5. convert to lower case
            str1 = str1.lower()
            str2 = str2.lower()

            # 6. detect Dutch
            dutchScore1 = self.dutch_score(str1)
            isDutch1 = self.is_dutch(dutchScore1)
            dutchScore2 = self.dutch_score(str2)
            isDutch2 = self.is_dutch(dutchScore2)

            # 7. extract potentially useful given tags
            tags = self.parse_tags(doc)

            # 8. save record
            self.clearData.append(
                {"docID": docID,
                 "simpleCorpus": str1,
                 "detailCorpus": str2,
                 "tags": {
                     "timestamp": doc["timestamp"],
                     "keywords": doc["keywords"],
                     "simpleIsDutch": isDutch1,
                     "simpleDutchScore": dutchScore1,
                     "detailIsDutch": isDutch2,
                     "detailDutchScore": dutchScore2,
                     # all key-value pairs in tags (python 3.5+)
                     **tags
                    }
                 })

    def extract_html_content(self, soup):
        """Extract data within assigned tags (single document)

        Args:
            soup (BeautifulSoup): the data

        Returns: string
        """

        # collect wanted contents and concatenate
        ls = []
        for tag in soup.find_all(self.keepTags):
            # debug
            # print(type(tag))
            # print(tag)
            # print(tag.string)
            if tag.string is not None:
                ls.append(tag.string)

        return " ".join(ls)

        # failed methods (stuck for so long...)

        # method 1: not working well (not stripping css)
        # text = soup.get_text()
        # self.textData.append(text)

        # method 2: remove all tags other than
        # https://stackoverflow.com/questions/10113702
        # also not working
        #
        # AttributeError: 'bytes' object has no attribute 'parent'
        # WTF?
        #
        # for tag in soup.find_all(True):
        #     print(f"[Debug] tag name = {tag.name}")
        #     if tag.name not in self.keepTags:
        #         new_tag = tag.encode_contents()
        #         tag.replace_with(new_tag)
        #
        # self.textData.append(soup.text)

    def remove_control_characters(self, text):
        """Cleanup some unwanted characters

        Args:
            text (string): text to be cleaned

        Returns: string

        """
        # remove control characters like \xa0
        reObj = re.compile(r"\\x\w{1,2}")
        return reObj.sub("", text)

    def remove_punctuations(self, text):
        """Remove punctuations

        Args:
            text (string): text with html tags removed

        Returns: string

        """
        # method 1: use native punctuation definition in strings
        # https://stackoverflow.com/questions/265960
        #return text.translate(str.maketrans("", "", string.punctuation))

        # method 2: TextBlob library
        blob = TextBlob(text)
        return " ".join(blob.words)


    @staticmethod
    def is_dutch(score):
        """
        Detect Dutch using Dutch-only words
        """
        if score > DUTCH_SCORE_THRESHOLD:
            return True
        else:
            return False

    def dutch_score(self, text):
        """
        Detect Dutch using Dutch-only words
        """

        if len(text) == 0:
            return 0
        else:
            # regex object to match Dutch words
            reObj = self.compile_regex_or(DUTCH_WORDS)
            # match count
            count = len(reObj.findall(text))

            return count / len(text)

    @staticmethod
    def compile_regex_or(wordlist, ignoreCase=True):
        """Compile regex OR expression from wordlist

        Args:
            wordlist (list): DUTCH_WORDS ['ga', 'en', ...]
            ignoreCase (bool): re.IGNORECASE flag

        Returns: _sre.SRE_Pattern
        """

        # concatenate list
        ls = ["("]
        for i, word in enumerate(wordlist):
            if i != (len(wordlist) - 1):
                ls.append("(" + fr"\W{word}\W" + ")|")
            else:  # last word
                ls.append("(" + fr"\W{word}\W" + "))")

        # compile regex
        if ignoreCase:
            return re.compile("".join(ls), re.IGNORECASE)
        else:
            return re.compile("".join(ls))

    def parse_tags(self, doc):
        """Retrieve tags from several locations

        Args:
            doc (dict): one raw record from mongodb

        Returns: dict

        """
        simple_tags = self.parse_simple_tags(doc)
        sidebar_tags = self.parse_sidebar_tags(doc)
        return {**simple_tags, **sidebar_tags}  # combine dicts (python 3.5+)


    def parse_simple_tags(self, doc):
        """Get useful tags from simpleData"""
        return {
            "education": doc["simpleData"]["educationLevel"],
            "jobTitle": doc["simpleData"]["title"],
            "workingHoursMin": doc["simpleData"]["workingHours"]["min"],
            "workingHoursMax": doc["simpleData"]["workingHours"]["max"],
            "workingLocation": doc["simpleData"]["workingLocation"],
        }

    def parse_sidebar_tags(self, doc):
        """Get useful tags from sidebarData"""

        salaryMin, salaryMax, salaryCurrency = self.parse_sidebar_salary(doc)

        return {
            "category": self.parse_sidebar_category(doc),
            "titles": self.parse_sidebar_titles(doc),
            "industry": self.parse_sidebar_industry(doc),
            "contractType": self.parse_sidebar_contractType(doc),
            "careerLevel": self.parse_sidebar_careerLevel(doc),
            "salaryMin": salaryMin,
            "salaryMax": salaryMax,
            "salaryCurrency": salaryCurrency,
        }

    def parse_sidebar_category(self, doc):

        ls = []
        # extract data
        # check data structure in mongodb: db.getCollection('nvb_raw').find({ $where: "this.sidebarData.category.length > 1" } ).count()
        text = doc["sidebarData"]["category"][0]
        soup = BeautifulSoup(text, "lxml")

        # append non-empty answers
        for s in soup.find_all("a"):
            if s is not None:
                ls.append(s.string.strip())

        return ls

    def parse_sidebar_titles(self, doc):

        ls_set = set()  # to remove duplicates

        # extract data
        text = doc["sidebarData"]["titles"][0]
        soup = BeautifulSoup(text, "lxml")

        # append non-empty answers
        for s in soup.find_all("a"):
            if s is not None:
                ls_set.add(s.string.strip())

        return list(ls_set)

    def parse_sidebar_industry(self, doc):

        ls = []
        text = doc["sidebarData"]["industry"][0]
        soup = BeautifulSoup(text, "lxml")

        for s in soup.find_all("a"):
            if s is not None:
                ls.append(s.string.strip())

        return ls

    def parse_sidebar_contractType(self, doc):

        ls = []
        text = doc["sidebarData"]["contractType"][0]
        soup = BeautifulSoup(text, "lxml")

        for s in soup.find_all("a"):
            if s is not None:
                ls.append(s.string.strip())

        return ls

    def parse_sidebar_careerLevel(self, doc):

        ls = []
        text = doc["sidebarData"]["careerLevel"][0]
        soup = BeautifulSoup(text, "lxml")

        for s in soup.find_all(itemprop="experienceRequirements"):
            if s is not None:
                ls.append(s.string.strip())

        return ls

    def parse_sidebar_salary(self, doc):

        if not doc["sidebarData"]["salary"]:
            return None, None, None

        text = doc["sidebarData"]["salary"][0]
        soup = BeautifulSoup(text, "lxml")

        salaryMin = soup.find(itemprop="minValue").get("content")
        salaryMax = soup.find(itemprop="maxValue").get("content")
        salaryCurrency = soup.find(itemprop="currency").get("content")

        return salaryMin, salaryMax, salaryCurrency

# workflow
if __name__ == '__main__':

    parser = NvbParser()
    parser.get_mongo(limit=0)
    parser.preprocess()
    parser.write_mongo()
    # check results in MongoDB!
