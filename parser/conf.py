# mongodb settings (require local mongodb installation)
MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DB = 'tue_wir'  # database name

# collection names
NVB_AGGR_RAW = 'nvb_aggregate_raw'
NVB_RAW = 'nvb_raw'
#NVB_PREPROCESSED = 'nvb_preprocessed'
NVB_PREPROCESSED = 'nvb_preprocessed_debug'

# tags to keep
KEEP_TAGS = ["p", "ul"]

# Dutch-only words (may obtain from real Dutch corpi)
DUTCH_WORDS = ["je", "nee", "niet", "geen", "aan", "het", "de", "een", "beetje", "om",
               "dit", "dat", "deze", "gevonden", "ga", "naar", "van", "op", "tot", "zoek",
               "gefeliciteerd", "opleiding", "envaren", "gegevens", "wetenschap",
               "onderzoek", "soliciteren",
               ]
# Score threshold to be considered Dutch! (empirically determined)
DUTCH_SCORE_THRESHOLD = 0.01

# logfile
OUT_DIR = "out"
LOGFILE = "main.log"
