.. tue-wir-jobscraper documentation master file, created by
   sphinx-quickstart on Fri Apr 26 15:34:05 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tue-wir-jobscraper documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   scraper
   parser


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
