# TU/e 2IMM15 Group 27 Final Project Workspace

Group Members:
* Chuan-Bin "Bill" Huang (1342037)
* Nishanth Ramanathan (1344358)
* Shrayasi Poddar (1329561)
* Berk Tamac (1236659)

The module analyzes job posts from [NVB job website](https://www.nationalevacaturebank.nl) with search keywords: (1) data analyst (2) data engineer (3) business analyst (4) statistical engineer

# Project Structure

## Main Modules (For Grading)
* `/scraper`: web scraper module (Chuan-Bin Huang)
* `/parser`: parser module (Chuan-Bin Huang)
* `/freq_miner`: frequency miner and word cloud (Nishanth Ramanathan)
* `/topic_modeler`: topic modeling (Shrayasi Poddar)
* `/vis`: visualization modules (Berk Tamac)

## Other Files
* `/changelog`: commit messages
* `/data`: shared data (MongoDB dumpfile)
* `/doc`: documentations
* `/img`: shared images
* `/TEMP`: files not to be committed (e.g. your own TODO or drafts)

# Prerequisites

* (common) `Python 3.6+`. Lower version cannot interpret f-string correctly.
* (common) A local `MongoDB` installation. The daemon must be enabled or
turned on before program execution.
* (common) A database `tue_wir` must be created,
or a dumpfile in `/data` must be imported.
* (scraper) Docker and a `Splash docker image`.
* (scraper) `Scrapy` the pythonic web scraping framework.
* (scraper and parser) `BeautifulSoup4` for XML parsing.

# Usage

## 1. Scraper

The scraper is based on python Scrapy framework. It was run on Debian 9 with the aid of Splash javascript rendering engine. The reason for using an additional wrapper of `run.sh` wrapper is to start/stop Splash before and after Scrapy. Execution:

```shell
  cd scraper/default
  bash run.sh
```

On windows it is also required to start Splash before scrapy but we did not bother to test so.

Retrieved data is saved into a collection named `nvb_raw` under the database `tue_wir` in MongoDB. Minimal preprocession is done for rigidity.

## 2. Parser

The parser is run after and independent of the scraper. Execution:

```shell
  cd parser
  python3 nvb.py
```

Configuration and adjustable parameters are stored in `conf.py` in order to separated data from logic.

The into a collection named `nvb_raw` under the database
`tue_wir`

## 3. Frequency Miner

```shell
  cd parser
  python3 word_cloud.py
```

The output is under `freq_miner/out`.

## 4. Topic Modeler

```shell
  cd parser
  python3 lda.py
```

The output is under `topic_moderer/out`.

# Screenshots


![Architecture](img/architecture.png)

## Scraped and Preprocessed Data

![Schema](img/mongo_schema.png)

## Word Cloud

![Data Analyst](img/da_eng.png)
![Data Analyst](img/ba_eng.png)

## Topic Modeling

Top topics from English job posts returned by the search keyword "data Anlayst".

![Data Analyst](img/lda_topic_1.png)
![Data Analyst](img/lda_topic_2.png)

# Development Notes (Not for grading)

## MongoDB Setup and Data Exchange

The setup process is verified on Linux Debian 9 and Win10.

* Install [MongoDB Server Community Edition](https://www.mongodb.com/download-center/community) and enable MongoDB daemon.
* Get the gzip mongodump file. Do not unzip.
* Open command prompt or console and do as follows

```shell
  # (Windows-only): switch to where your local mongodb installation locates
  C:
  cd "C:\Program Files\MongoDB\Server\4.0\bin"

  # import (restore) db from a gzip file
  mongorestore --gzip --archive="D:\GitLab\tue-wir-jobscraper\TEMP\tue_wir.20190310.gz" --db tue_wir

  # export (dump) db to a gzip file
  mongodump --gzip --archive="D:\GitLab\tue-wir-jobscraper\TEMP\tue_wir.20190310.gz" --db tue_wir
```

On Win10 you have "MongoDB Compass" as your built-in GUI:

![MongoDB_GUI](img/win10_mongodb.png)

## Scrapy Project Setup

* Requirements:
```bash
  # NOTE: which pip3

  # using anaconda (recommended)
  conda install -c scrapinghub scrapy
  # using system python
  sudo pip3 install Scrapy
```

* Create project:
```bash
  # cd to the right place and run
  scrapy startproject my_project
```

## Running Scrapy with Splash for AJAX Pages

* Requirements:
```bash
  pip3 install scrapy
  # for javascript pages
  pip3 install scrapy-splash
  # splash engine
  sudo apt install docker  # or see official docker site for the latest version
  docker pull scrapinghub/splash
```

* Add to scrapy `settings.py`:
```python
  SPLASH_URL = 'http://localhost:8050'
  DOWNLOADER_MIDDLEWARES = {
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
  }
  SPIDER_MIDDLEWARES = {
    'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
  }
  DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'
```

* Run before scrapy:
```bash
  docker run -p 8050:8050 scrapinghub/splash
```

Or create a bash runner script that launches docker before scrapy is run
and shuts it down after scrapy is finished.

## Version Control

- **gitignore / gitkeep policy**:
I basically followed [this approach](https://davidwalsh.name/git-empty-directory) and [this outdated (?) approach](https://stackoverflow.com/a/20388370/3218693). `.gitignore` and `.gitkeep` are used jointly.

- **Commit Message**:
You may consider writing a log file in `changelog/` and then commit by
the following command instead of `git commit -m`:

```bash
  # commit your prepared message
  git commit -eF "changelog/YYYY-MM-DD.HHMM.txt"
  # push to GitLab
  git push
```

- **Editor** Bill recommends [Atom](https://atom.io) text editor.

  * Cam preview while editing by simply pressing `Ctrl-Shift-M` with the
`Markdown Preview` package installed.

  * Can use an integrated console window by pressing `Ctrl-``

    ![atom](img/atom.png? "Atom Interface")

## Sphinx Documentation

There is a sphinx documentation generated in `doc/_build/html`. One can view by

```bash
  cd doc/_build/html
  python3 -m http.server
  # and open http://localhost:8000 in your browser
```

![Sphinx](img/sphinx.png)
